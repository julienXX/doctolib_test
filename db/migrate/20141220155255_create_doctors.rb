class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :first_name
      t.string :last_name
      t.string :address_line1
      t.string :address_line2
      t.integer :zipcode
      t.string :city
      t.string :speciality

      t.timestamps null: false
    end
  end
end
