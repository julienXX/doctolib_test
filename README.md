# Test Doctolib

## Setup:

```
$ git clone git@bitbucket.org:julienXX/doctolib_test.git
$ cd doctolib_test
$ bundle install
$ bundle exec rails s
```

Enfin ouvrir un navigateur sur ```http://localhost:3000```.

## Tests:
```
$ rake test
```

## Notes:
J'ai mis environ 1h15 pour le CRUD et 1h30 pour créer la classe Event.
