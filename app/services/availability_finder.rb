class AvailabilityFinder
  attr_reader :date

  def initialize(date: date)
    @date = date
  end

  def all
    [].tap do |availabilities|
      week(date).each do |day|
        availability = { date: day, slots: available_slots(day) }
        availabilities << availability
      end
    end
  end

  private

  def available_slots(day)
    return [] if day.wday == 0
    all_slots - occupied_slots(day)
  end

  def all_slots
    [].tap do |slots|
      start = Event.opening.starts_at.to_i
      stop = Event.opening.ends_at.to_i - 1800

      (start..stop).step(1800).each do |timestamp|
        slots << Time.zone.at(timestamp).strftime('%H:%M')
      end
    end
  end

  def occupied_slots(day)
    appointments = appointments_for(day)

    [].tap do |slots|
      appointments.each do |appointment|
        start = appointment.starts_at.to_i
        stop = appointment.ends_at.to_i - 1800

        (start..stop).step(1800).each do |timestamp|
          slots << Time.zone.at(timestamp).strftime('%H:%M')
        end
      end
    end
  end

  def appointments_for(date)
    # Only valid with SQLite
    Event.appointments.where("strftime('%m/%d', starts_at) = ?", date.strftime('%m/%d'))
  end

  def week(date)
    Range.new(date, date + 6.days)
  end
end
