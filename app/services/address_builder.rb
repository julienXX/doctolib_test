class AddressBuilder
  attr_reader :line1, :line2, :zipcode, :city

  def initialize(doctor: doctor)
    @doctor = doctor
    @line1 = doctor.address_line1
    @line2 = doctor.address_line2
    @zipcode = doctor.zipcode
    @city = doctor.city
  end

  def build
    address_lines.join(' ')
  end

  def address_lines
    [line1, line2, zipcode, city].compact
  end
end
