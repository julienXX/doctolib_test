# -*- coding: utf-8 -*-
class DoctorsController < ApplicationController

  before_action :set_doctor, only: [:show, :edit, :update, :destroy]

  def index
    @doctors = Doctor.all

    @markers = Gmaps4rails.build_markers(@doctors) do |doctor, marker|
      marker.lat doctor.latitude
      marker.lng doctor.longitude
    end
  end

  def show
    @markers = Gmaps4rails.build_markers(@doctor) do |doctor, marker|
      marker.lat doctor.latitude
      marker.lng doctor.longitude
    end
  end

  def new
    @doctor = Doctor.new
  end

  def edit
  end

  def create
    @doctor = Doctor.new(doctor_params)

    respond_to do |format|
      if @doctor.save
        format.html { redirect_to doctors_path, notice: 'Docteur ajouté.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @doctor.update(doctor_params)
        format.html { redirect_to doctors_path, notice: 'Docteur mis à jour.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @doctor.destroy
    respond_to do |format|
      format.html { redirect_to doctors_url, notice: 'Docteur supprimé.' }
    end
  end

  private

  def set_doctor
    @doctor = Doctor.find(params[:id])
  end

  def doctor_params
    params.require(:doctor)
      .permit(:first_name, :last_name, :address_line1, :address_line2, :zipcode, :city, :speciality)
  end
end
