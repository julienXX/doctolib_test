class Doctor < ActiveRecord::Base
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :address_line1, presence: true
  validates :zipcode, presence: true
  validates :city, presence: true
  validates :speciality, presence: true

  geocoded_by :address
  after_validation :geocode

  def address
    AddressBuilder.new(doctor: self).build
  end

  def full_name
    [first_name, last_name].map(&:capitalize).join(' ')
  end
end
