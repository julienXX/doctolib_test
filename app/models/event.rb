class Event < ActiveRecord::Base
  scope :opening,      -> { where(kind: 'opening').first }
  scope :appointments, -> { where(kind: 'appointment') }

  class << self
    def availabilities(date = nil)
      return [] unless date
      AvailabilityFinder.new(date: date).all
    end
  end
end
